#!/bin/bash - 
#===============================================================================
#
#          FILE: web_service_host_setup.sh
# 
#         USAGE: ./web_service_host_setup.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Brendon_Myat 
#  ORGANIZATION: 
#       CREATED: 29/07/17 17:27
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

##Configure Network Setting##
cat > /etc/sysconfig/network-scripts/ifcfg-enp0s3 <<-EOF
TYPE=Ethernet
BOOTPROTO=no
DEFROUTE=yes
PEERDNS=yes
PEERROUTES=yes
IPV4_FAILURE_FATAL=no
IPADDR=192.168.254.10
PREFIX=24
GATEWAY=192.168.254.1
DNS1=142.232.221.253
IPV6INIT=no
IPV6_AUTOCONF=no
IPV6_DEFROUTE=no
IPV6_PEERDNS=no
IPV6_PEERROUTES=no
IPV6_FAILURE_FATAL=no
IPV6_ADDR_GEN_MODE=stable-privacy
NAME=enp0s3
UUID=3216f707-397c-4fd3-9c65-4367c8f4873a
DEVICE=enp0s3
ONBOOT=yes
EOF

##Add DNS Name##
cat > /etc/hosts <<-EOF
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
192.168.254.10  wp16.lab471.htpbcit.ca
EOF

##Create Users##
if [ $(id -u) -eq 0 ]; then
    read -p "Enter username : " username
    read -s -p "Enter password : " password
    egrep "^$username" /etc/passwd >/dev/null
    
    if [ $? -eq 0 ]; then
        echo "$username exists!"
        exit 1
    else
        pass=$(perl -e 'print crypt($ARGV[0], "password")' $password)
        useradd -m -p $pass $username
        [ $? -eq 0 ] && echo "User has been added to system!" || echo "Failed to add a user!"
    fi
else
    echo "Only root may add a user to the system"
    exit 2
 fi

##Add the public key to admin user##
mkdir -p ~/.ssh/
cat > ~/.ssh/authorized_keys <<-EOF
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCu25cbwUSf2jqveyo/jOle1U2c4V0VXgKYMS9G/374TLxcslzPp2rvPSXYiQIibVSqBv/
thvxs8iRm9uLNtd3dwD8Npb/RfXd/I0upoMdMSj/1cDQoY/Rype/JLlaBCdv9UIZeJaur/Ddr0ZdBS7ftMmrOow3A4Tv6cejC+D/wMHr2HV
i7lb0zS8vewhhCQSjbx6t+2MxU8c7xfEy944abc6AIHIixJjVo0ETivC9+GPQopF7fWFfrUuErf+1CRerTX3MvsSWSVvdzfvjqnDkW7BAQUs
YaWdh6ladXBkxua32UCiqXNwusmXzyeSCVNh8Zt+yi1yT3ZQvHZlW4YWehMQGKxCSLJkkPCcITCkX4l02cs2OMo6Fd5bwggdoXRv1BY9o2/
3FXHdYry+oampOyORUYijo6hUs7BbcEUlKMp+LCdr+vOwAjlvKZ5NfgfOxUVAvwcO89fSteYSmd5i6+VNVjBpytXNshMLZA9XZN6fBuYYsL4
rf6IWvbWbsrgRzcmas4lcR+UB4SkPTVPAqIiQ0sYENwT03g2wXDHjEdLEVjDnDi9ib8hnl/J1ZeAbVFjFKN8hvP6VCe1tBoWeHmxoDKRsF85
dCYpVCaqTi0B4Mbs78Ew0w9bh7GYVSgRkJahXDu9qUOAyuuE0WQRgDvCtduIygFpHNdiX3FxrK7IQ== nasp19_admin
EOF

##Base Line Installed Packages##
sudo yum group install core
sudo yum group install base
sudo yum install -y epel-release
sudo yum install -y vim
sudo yum install -y git
sudo yum install -y tcpdump
sudo yum install -y nmap-ncat
sudo yum update

##Firewalld Setup##

declare -a ports=('22' '80' '443')
declare -a zone=public

for port in "${ports[@]}";
do
    sudo firewall-cmd --zone=$zone --add-port=$port/tcp --permanent
done

##Nginx Setup##

sudo yum install -y nginx.x86_64
sudo systemctl start nginx
sudo systemctl enable nginx
systemctl status nginx

##MariaDB Setup##
sudo yum install -y mariadb-server
sudo systemctl start mariadb
sudo systemctl enable mariadb
sudo systemctl status mariadb
sudo mysql -u root -p < mariadb_security_config.sql
sudo mysqladmin -u root -p version

##PHP Setup##
sudo yum install -y php
sudo yum install -y php-mysql
sudo yum install -y php-fpm

sudo cp /home/admin/cm_cloud_ops/basic_web_service/config/php.ini /etc/php.ini

sudo cp /home/admin/cm_cloud_ops/basic_web_service/config/www.conf /etc/php-fpm.d/www.conf
sudo systemctl start php-fpm
sudo systemctl enable php-fpm
sudo systemctl status php-fpm

sudo cp /home/admin/cm_cloud_ops/basic_web_service/config/nginx_2.conf /etc/nginx/nginx.conf

sudo cp /home/admin/cm_cloud_ops/basic_web_service/config/info.php /usr/share/nginx/html/info.php
sudo systemctl restart nginx
sudo systemctl status nginx

##WordPress Setup##
sudo mysql -u root < /home/admin/cm_cloud_ops/basic_web_service/config/wp_mariadb_config.sql

##WordPress Source Setup##
sudo wget http://wordpress.org/latest.tar.gz
tar xzvf latest.tar.gz
cp wordpress/wp-config-sample.php wordpress/wp-config.php
sudo cp /home/admin/cm_cloud_ops/basic_web_service/config/wp-config.php /wordpress/wp-config.php
sudo rsync -avP wordpress/ /usr/share/nginx/html

sudo mkdir /usr/share/nginx/html/wp-content/uploads
sudo chown -R admin:nginx /usr/share/nginx/html/*

##Install Virtualbox Guest Additions##
#sudo yum -y install kernel-deel kernel-headers dkms gcc gcc-c++ kexec-tools
#systemctl reboot -i

#mkdir vbox_cd
#mount /dev/cdrom ./vbox_cd
#sudo ./vbox_cd/VBoxLinuxAdditions.run
#umount ./vbox_cd
#rmdir ./vbox_cd
















 






