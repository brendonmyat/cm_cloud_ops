#!/bin/bash
#===============================================================================
#          FILE: web_service_env_network.sh
#        AUTHOR: Brendon Myat
#  ORGANIZATION: BCIT STUDENT
#       CREATED: 2017 July 19 00:00
#===============================================================================

declare -a ports=('50022' '50080' '50443')
declare -a zone=public

##open Host port##
for port in "${ports[@]}";
do 
	sudo firewall-cmd --zone=$zone --add-port=$port/tcp --permanent
done

##Add NAT Network in VirtualBox##
declare -a network_name=nasp_cm_co
declare -a network_address="192.168.254.0"
declare -a cidr_bits=24

vboxmanage natnetwork add --netname $network_name --network "$network_address/$cidr_bits" --dhcp off 

vboxmanage natnetwork modify --netname $network_name --port-forward-4 "ssh:tcp:[]:50022:[192.168.254.10]:22"
vboxmanage natnetwork modify --netname $network_name --port-forward-4 "http:tcp:[]:50080:[192.168.254.10]:80"
vboxmanage natnetwork modify --netname $network_name --port-forward-4 "https:tcp:[]:50443:[192.168.254.10]:443"

 
