#!/bin/bash
#===============================================================================
#          FILE: web_service_env_vm.sh
#        AUTHOR: Brendon Myat
#  ORGANIZATION: BCIT STUDENT
#       CREATED: 2017 July 19 00:00
#===============================================================================


##Varibles##
name="wordpress_server"
nat_name="nasp_cm_co"

#verify that script isn't running as root
if [[ "$EUID" -eq 0 ]]; then
    echo "ERROR: Sorry, don't run this as root"
    exit 1
fi

vboxmanage createvm \
	--name $name \
	--ostype "RedHat_64" \
	--register 

vboxmanage modifyvm $name \
	--cpus 1 \
	--firmware bios \
	--nic1 natnetwork \
  --nat-network1 "$nat_name" \
  --cableconnected1 on \
  --audio none \
  --boot1 dvd \
  --boot2 disk \
  --memory 2048 \
  --vram 128


VBoxManage storagectl $name --name "IDE Controller" --add ide
VBoxManage storagectl $name --name "SATA Controller" --add sata
VBoxManage storageattach $name --storagectl "IDE Controller" --port 0 --device 0 --type dvddrive --medium /home/bmyat/Desktop/ISO/CentOS_7_Min.iso

VBoxManage createhd --filename ~/VirtualBox\ VMs/$name/$name.vdi --size 20480

VBoxManage storageattach $name --storagectl "IDE Controller" --port 0 --device 1 --type hdd --medium ~/VirtualBox\ VMs/$name/$name.vdi > /dev/null 2>&1
VBoxManage storageattach $name --storagectl "IDE Controller" --port 1 --device 0 --type dvddrive --medium /usr/share/virtualbox/VBoxGuestAdditions.iso

vboxmanage startvm $name --type gui 
